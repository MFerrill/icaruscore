local NPC_ID = 130004 -- Invisible Word Trigger 
local Teleport_Range = 1 -- Range in yards
     
function AreaTrigger_OnSpawn(event, creature)
    creature:SetVisible(false)
    creature:SetFaction(35)
end
   
function AreaTrigger_MoveInLOS(event, creature, plr)
        if (plr:GetName() and creature:IsWithinDistInMap(plr, Teleport_Range)) then
                plr:Teleport(870, -214.651, -5308.222, 186, 4.523) -- Platform Alpha
        end
end
  
RegisterCreatureEvent(NPC_ID, 5, AreaTrigger_OnSpawn)
RegisterCreatureEvent(NPC_ID, 27, AreaTrigger_MoveInLOS)
