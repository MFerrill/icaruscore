-- #############################
-- ##    Shared by RazorX14   ##
-- ##       For BotCore       ##
-- #############################


--> GUILD HOUSE VENDOR CODE

local VENDOR_ID = 80003
local ITEM_ID = 510016


--> DO NOT ALTER ANYTHING BELOW THIS POINT!!!

local function GetGUILD_COST_SMALL(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_COST_SMALL from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_COST_LARGE(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_COST_LARGE from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_COST_LARGE2(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_COST_LARGE2 from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_COST_LARGE3(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_COST_LARGE3 from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGM_GUILD_COST(playername)
	local CHECK = WorldDBQuery("SELECT GM_GUILD_COST from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_SELL_SMALL(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_SELL_SMALL from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_SELL_LARGE(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_SELL_LARGE from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_SELL_LARGE2(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_SELL_LARGE2 from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGUILD_SELL_LARGE3(playername)
	local CHECK = WorldDBQuery("SELECT GUILD_SELL_LARGE3 from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function GetGM_GUILD_SELL(playername)
	local CHECK = WorldDBQuery("SELECT GM_GUILD_SELL from guild_houses_prices where id = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end

-- Door GUID's
local Door_GM = 206539
local Door_01 = 216171
local Door_02 = 216177
local Door_03 = 216194
local Door_04 = 216201
local Door_05 = 216204
local Door_06 = 216208
local Door_07 = 216209
local Door_08 = 216210
local Door_09 = 216211
local Door_10 = 216212
local Door_11 = 217140
local Door_12 = 217142
local Door_13 = 217144
local Door_14 = 217146
local Door_15 = 217148
local Door_16 = 217138
local Door_17 = 217136
local Door_18 = 217134
local Door_19 = 217132
local Door_20 = 217130

-- Kicker GUID's
local Kicker_GM = 215195
local Kicker_01 = 216168
local Kicker_02 = 216191
local Kicker_03 = 216195
local Kicker_04 = 216203
local Kicker_05 = 216207
local Kicker_06 = 216213
local Kicker_07 = 216214
local Kicker_08 = 216215
local Kicker_09 = 216216
local Kicker_10 = 216217
local Kicker_11 = 217251
local Kicker_12 = 217252
local Kicker_13 = 217253
local Kicker_14 = 217254
local Kicker_15 = 217255
local Kicker_16 = 217256
local Kicker_17 = 217257
local Kicker_18 = 217258
local Kicker_19 = 217259
local Kicker_20 = 217260

local function GrantGuildHouse1(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='1' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_GM .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_GM .. " where Guid='" .. guid .. "'")
end
local function GrantGuildHouse2(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='2' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_01 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_01 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=1")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=1")
end
local function GrantGuildHouse3(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='3' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_02 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_02 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=2")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=2")
end
local function GrantGuildHouse4(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='4' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_03 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_03 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=3")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=3")
end
local function GrantGuildHouse5(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='5' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_04 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_04 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=4")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=4")
end
local function GrantGuildHouse6(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='6' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_05 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_05 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=5")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=5")
end
local function GrantGuildHouse7(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='7' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_06 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_06 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=6")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=6")
end
local function GrantGuildHouse8(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='8' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_07 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_07 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=7")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=7")
end
local function GrantGuildHouse9(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='9' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_08 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_08 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=8")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=8")
end
local function GrantGuildHouse10(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='10' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_09 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_09 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=9")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=9")
end
local function GrantGuildHouse11(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='11' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_10 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_10 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=10")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=10")
end
local function GrantGuildHouse12(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='12' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_11 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_11 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=11")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=11")
end
local function GrantGuildHouse13(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='13' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_12 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_12 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=12")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=12")
end
local function GrantGuildHouse14(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='14' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_13 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_13 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=13")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=13")
end
local function GrantGuildHouse15(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='15' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_14 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_14 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=14")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=14")
end
local function GrantGuildHouse16(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='16' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_15 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_15 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=15")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=15")
end
local function GrantGuildHouse17(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='17' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_16 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_16 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=16")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=16")
end
local function GrantGuildHouse18(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='18' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_17 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_17 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=17")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=17")
end
local function GrantGuildHouse19(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='19' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_18 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_18 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=18")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=18")
end
local function GrantGuildHouse20(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='20' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_19 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_19 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=19")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=19")
end
local function GrantGuildHouse21(playername, guid)
	CharDBQuery("INSERT INTO `world2`.`guild_houses`(`Guild`,`GuildId`) SELECT `guild`.`name`, `guild`.`guildid` FROM `guild` where leaderguid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET Guid=" .. guid .. " where Guid=0")
	WorldDBQuery("UPDATE guild_houses SET PlayerName='" .. playername .. "' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET GuildHouse='21' where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET DoorGuid=" .. Door_20 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses SET KickerGuid=" .. Kicker_20 .. " where Guid='" .. guid .. "'")
	WorldDBQuery("UPDATE guild_houses_vendor1_locs SET PlayerName='" .. playername .. "' where id=20")
	WorldDBQuery("UPDATE guild_houses_vendor2_locs SET PlayerName='" .. playername .. "' where id=20")
end


local function Check_If_Bought1(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 1")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought2(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 2")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought3(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 3")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought4(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 4")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought5(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 5")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought6(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 6")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought7(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 7")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought8(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 8")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought9(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 9")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought10(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 10")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought11(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 11")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought12(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 12")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought13(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 13")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought14(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 14")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought15(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 15")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought16(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 16")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought17(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 17")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought18(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 18")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought19(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 19")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought20(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 20")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end
local function Check_If_Bought21(playername)
	local CHECK = WorldDBQuery("SELECT GuildHouse from guild_houses where GuildHouse = 21")
	local value = 0
	if(CHECK) then
        repeat
            value = CHECK:GetUInt32(0)
        until not CHECK:NextRow()
	end
    return value
end


local function SellGuildHouse(playername, guid)
	local SGuildHouse = WorldDBQuery("DELETE from guild_houses where PlayerName='" .. playername .. "'")
end


local function GetGuildHouse(playername)
local GuildHouse = WorldDBQuery("SELECT `GuildHouse` from `guild_houses` where `PlayerName`='" .. playername .. "'")
	local house = 0
	if(GuildHouse) then
		repeat
			house = GuildHouse:GetUInt32(0)
		until not GuildHouse:NextRow()
	end
	return house
end


local function GetGUID(playername)
	local GUID = CharDBQuery("SELECT `guid` from `characters` where `name`='" .. playername .. "'")
	local id = 0
	if(GUID) then
        repeat
            id = GUID:GetUInt32(0)
        until not GUID:NextRow()
	end
    return id
end


local function GetGuildIdFromGuildHouses(guildid)
	local GuildId = WorldDBQuery("SELECT `GuildId` FROM `guild_houses` WHERE `GuildId` LIKE " .. guildid .. "")
    if(GuildId) then
			gguid = GuildId:GetUInt32(0)
    return gguid
	end
	return false
end

local function On_GossipMenu(event, player, unit, sender, intid, code, popup, money)
	local PlrName = player:GetName()
	if (player:GetGuildName() == NULL) then
		player:SendNotification("You Dont Have A Guild "..PlrName.."")
		player:GossipComplete()
	   return
	else
        player:GossipMenuAddItem(0, "Guild Houses", 0, 1, 0)
		player:GossipMenuAddItem(6, "Sell Guild House", 0, 2000, 0, "Are You Sure You Wish To Sell?")
		player:GossipMenuAddItem(1, "Buy 15 [Solid Gold Coin]", 0, 8000, 0, "Buy 15 [Solid Gold Coin] For", "150000000")
		player:GossipSendMenu(1, unit)
	end
end

local function GetGuildFromGuildHouses1(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 1")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses2(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 2")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses3(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 3")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses4(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 4")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses5(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 5")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses6(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 6")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses7(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 7")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses8(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 8")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses9(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 9")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses10(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 10")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses11(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 11")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses12(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 12")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses13(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 13")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses14(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 14")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses15(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 15")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses16(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 16")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses17(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 17")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses18(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 18")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses19(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 19")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses20(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 20")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end
local function GetGuildFromGuildHouses21(playername)
	local Guild = WorldDBQuery("SELECT `Guild` FROM `guild_houses` WHERE `GuildHouse` = 21")
    if(Guild) then
			guild = Guild:GetString(0)
    return guild
	end
	return "Unsold"
end

local function GetPublicHouseOwner(playername)
	local PublicHouseOwner = WorldDBQuery("SELECT `PlayerName` FROM `public_houses` where `PlayerName`='" .. playername .. "'")
    if(PublicHouseOwner) then
			PublicHouseOwner = PublicHouseOwner:GetString(0)
    return true
	end
	return false
end

local function On_GossipSelect(event, player, unit, sender, intid, code, popup, money)
		local PlrName = player:GetName()
	if (intid == 2000) then
		if player:HasTitle(3202) then
			player:UnsetKnownTitle(3202)
			player:ModifyMoney(100000)
		end
		if GetGuildHouse(PlrName) == 1 then
			if Check_If_Bought1(player:GetName()) == 1 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGM_GUILD_SELL(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 2 then
			if Check_If_Bought2(player:GetName()) == 2 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 1")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 1")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 3 then
			if Check_If_Bought3(player:GetName()) == 3 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 2")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 2")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 4 then
			if Check_If_Bought4(player:GetName()) == 4 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 3")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 3")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 5 then
			if Check_If_Bought5(player:GetName()) == 5 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 4")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 4")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 6 then
			if Check_If_Bought6(player:GetName()) == 6 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 5")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 5")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 7 then
			if Check_If_Bought7(player:GetName()) == 7 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 6")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 6")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 8 then
			if Check_If_Bought8(player:GetName()) == 8 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 7")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 7")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 9 then
			if Check_If_Bought9(player:GetName()) == 9 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 8")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 8")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 10 then
			if Check_If_Bought10(player:GetName()) == 10 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 9")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 9")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 11 then
			if Check_If_Bought11(player:GetName()) == 11 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 10")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 10")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 12 then
			if Check_If_Bought12(player:GetName()) == 12 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 11")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 11")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 13 then
			if Check_If_Bought13(player:GetName()) == 13 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 12")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 12")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 14 then
			if Check_If_Bought14(player:GetName()) == 14 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 13")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 13")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 15 then
			if Check_If_Bought15(player:GetName()) == 15 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 14")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 14")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 16 then
			if Check_If_Bought16(player:GetName()) == 16 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 15")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 15")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 17 then
			if Check_If_Bought17(player:GetName()) == 17 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 16")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 16")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 18 then
			if Check_If_Bought18(player:GetName()) == 18 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 17")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 17")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 19 then
			if Check_If_Bought19(player:GetName()) == 19 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 18")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 18")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 20 then
			if Check_If_Bought20(player:GetName()) == 20 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 19")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 19")
			player:GossipComplete()
			return
			end
		end
		if GetGuildHouse(PlrName) == 21 then
			if Check_If_Bought21(player:GetName()) == 21 then
			player:SendNotification("Your Guild House Has Now Been Sold "..PlrName.."")
			SellGuildHouse(player:GetName())
			player:AddItem(ITEM_ID, GetGUILD_SELL_LARGE(player:GetName()))
			SendWorldMessage(""..player:GetGuildName().. " Has Just Sold There Guild House..")
			WorldDBQuery("UPDATE `guild_houses_vendor1_locs` SET PlayerName = NULL WHERE `id` = 20")
			WorldDBQuery("UPDATE `guild_houses_vendor2_locs` SET PlayerName = NULL WHERE `id` = 20")
			player:GossipComplete()
			return
			end
		else
			player:SendNotification("You Do Not Own A Guild House "..PlrName.."")
			player:GossipComplete()
			return
		end
		if player:HasTitle(3202) then
			player:UnsetKnownTitle(3202)  -- Remove Homeowner Title
			player:ModifyMoney(100000)
			player:GossipComplete()
		end
	end
	if(intid == 1) then
	if GetGuildIdFromGuildHouses(player:GetGuildId()) then
	player:SendNotification("You Already Have A Guild House "..PlrName.."")
	   player:GossipComplete()
	   return
	end
	if GetPublicHouseOwner(player:GetName()) then
		player:SendNotification("You Cannot Own A PublicHouse And A GuildHouse "..PlrName.."")
		player:GossipComplete()
		return
	end
		player:GossipMenuAddItem(1, "[GM] "..GetGuildFromGuildHouses1(player:GetName()).." Guild House, "..GetGM_GUILD_COST(player:GetName()).." Coins", 0, 3, 0)
		player:GossipMenuAddItem(1, "[01] "..GetGuildFromGuildHouses2(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 4, 0)
		player:GossipMenuAddItem(1, "[02] "..GetGuildFromGuildHouses3(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 5, 0)
		player:GossipMenuAddItem(1, "[03] "..GetGuildFromGuildHouses4(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 6, 0)
		player:GossipMenuAddItem(1, "[04] "..GetGuildFromGuildHouses5(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 7, 0)
		player:GossipMenuAddItem(1, "[05] "..GetGuildFromGuildHouses6(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 8, 0)
		player:GossipMenuAddItem(1, "[06] "..GetGuildFromGuildHouses7(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 9, 0)
		player:GossipMenuAddItem(1, "[07] "..GetGuildFromGuildHouses8(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 10, 0)
		player:GossipMenuAddItem(1, "[08] "..GetGuildFromGuildHouses9(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 11, 0)
		player:GossipMenuAddItem(1, "[09] "..GetGuildFromGuildHouses10(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 12, 0)
		player:GossipMenuAddItem(1, "[10] "..GetGuildFromGuildHouses11(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 13, 0)
		player:GossipMenuAddItem(1, "[11] "..GetGuildFromGuildHouses12(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 14, 0)
		player:GossipMenuAddItem(1, "[12] "..GetGuildFromGuildHouses13(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 15, 0)
		player:GossipMenuAddItem(1, "[13] "..GetGuildFromGuildHouses14(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 16, 0)
		player:GossipMenuAddItem(1, "[14] "..GetGuildFromGuildHouses15(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 17, 0)
		player:GossipMenuAddItem(1, "[15] "..GetGuildFromGuildHouses16(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 18, 0)
		player:GossipMenuAddItem(1, "[16] "..GetGuildFromGuildHouses17(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 19, 0)
		player:GossipMenuAddItem(1, "[17] "..GetGuildFromGuildHouses18(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 20, 0)
		player:GossipMenuAddItem(1, "[18] "..GetGuildFromGuildHouses19(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 21, 0)
		player:GossipMenuAddItem(1, "[19] "..GetGuildFromGuildHouses20(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 22, 0)
		player:GossipMenuAddItem(1, "[20] "..GetGuildFromGuildHouses21(player:GetName()).." Guild House, "..GetGUILD_COST_LARGE(player:GetName()).." Coins", 0, 23, 0)		
		player:GossipMenuAddItem(0, "Back", 0, 1000)
		player:GossipSendMenu(1, unit)
	elseif(intid == 1000) then
		player:GossipMenuAddItem(0, "Guild Houses", 0, 1, 0)
		player:GossipMenuAddItem(6, "Sell Guild House", 0, 2000, 0, "Are You Sure You Wish To Sell?")
		player:GossipMenuAddItem(1, "Buy 15 [Solid Gold Coin]", 0, 8000, 0, "Buy [Solid Gold Coin] For", "150000000")
		player:GossipSendMenu(1, unit)
	elseif(intid == 8000) then
	if (player:GetCoinage() >= 150000000) then
		player:SendAreaTriggerMessage("You Have Bought a 15 [Solid Gold Coin]")
		player:AddItem(ITEM_ID, 15)
		player:ModifyMoney(-150000000)
		player:GossipComplete()
		return
		else
		player:SendAreaTriggerMessage("You Dont Have Enough Money")
		player:GossipComplete()
		return
		end
	elseif (intid == 3) then
		if Check_If_Bought1(player:GetName()) >= 1 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGM_GUILD_COST(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought The GM Guild House..")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGM_GUILD_COST(player:GetName()))
			GrantGuildHouse1(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGM_GUILD_COST(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 4) then
		if Check_If_Bought2(player:GetName()) >= 2 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 1")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse2(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 5) then
		if Check_If_Bought3(player:GetName()) >= 3 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 2")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse3(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 6) then
		if Check_If_Bought4(player:GetName()) >= 4 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 3")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse4(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 7) then
		if Check_If_Bought5(player:GetName()) >= 5 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 4")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse5(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 8) then
		if Check_If_Bought6(player:GetName()) >= 6 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 5")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse6(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 9) then
		if Check_If_Bought7(player:GetName()) >= 7 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 6")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse7(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 10) then
		if Check_If_Bought8(player:GetName()) >= 8 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 7")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse8(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 11) then
		if Check_If_Bought9(player:GetName()) >= 9 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 8")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse9(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 12) then
		if Check_If_Bought10(player:GetName()) >= 10 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 9")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse10(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 13) then
		if Check_If_Bought11(player:GetName()) >= 11 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 10")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse11(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 14) then
		if Check_If_Bought12(player:GetName()) >= 12 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 11")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse12(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 15) then
		if Check_If_Bought13(player:GetName()) >= 13 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 12")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse13(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 16) then
		if Check_If_Bought14(player:GetName()) >= 14 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 13")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse14(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 17) then
		if Check_If_Bought15(player:GetName()) >= 15 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 14")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse15(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 18) then
		if Check_If_Bought16(player:GetName()) >= 16 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 15")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse16(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 19) then
		if Check_If_Bought17(player:GetName()) >= 17 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 16")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse17(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 20) then
		if Check_If_Bought18(player:GetName()) >= 18 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 17")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse18(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 21) then
		if Check_If_Bought19(player:GetName()) >= 19 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 18")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse19(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 22) then
		if Check_If_Bought20(player:GetName()) >= 20 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 19")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse20(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	elseif (intid == 23) then
		if Check_If_Bought21(player:GetName()) >= 21 then
			player:SendNotification("Someone Owns This Guild House!")
			player:GossipComplete()
			elseif (player:HasItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))) then
			player:SendAreaTriggerMessage("You Now Own This Guild House "..PlrName.."")
			SendWorldMessage(""..player:GetGuildName().. " Has Just Bought Guild House Number 20")
			local PlrName = player:GetName()
			local TargetGUID = GetGUID(player:GetName())
			player:RemoveItem(ITEM_ID, GetGUILD_COST_LARGE(player:GetName()))
			GrantGuildHouse21(PlrName, TargetGUID)
			player:GossipComplete()
			else
			player:SendNotification("You Dont Have Enough, You Need "..GetGUILD_COST_LARGE(player:GetName()).." Coins")
			player:GossipComplete()
		return
		end
	end
end

RegisterCreatureGossipEvent(VENDOR_ID, 1, On_GossipMenu)
RegisterCreatureGossipEvent(VENDOR_ID, 2, On_GossipSelect)


--< GUILD HOUSE VENDOR CODE