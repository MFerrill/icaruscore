local function GetKickerOwner(playername)
	local plrOwnsKicker = WorldDBQuery("SELECT KickerGuid FROM public_houses WHERE PlayerName='" .. playername .. "'")
    if(plrOwnsKicker) then
			plrOwnsKicker = plrOwnsKicker:GetUInt32(0)
    return plrOwnsKicker
	end
	return false
end

function customObj_GossipMenu(event, player, object)
	local clickedKicker = object:GetGUIDLow()
	local PlrName = player:GetName()
	if clickedKicker == GetKickerOwner(PlrName) then
		player:GossipClearMenu()
		player:GossipMenuAddItem(2, 'Kick Player From House', 0, 1, true, "Click Accept then enter the name of the player you want to Kick.")
		player:GossipMenuAddItem(0, "Close Menu", 0, 1000)
		player:GossipSendMenu(1, object)
	else
		player:SendNotification("That Doesnt Belong To You..")
		player:GossipComplete()
	end
end

RegisterGameObjectGossipEvent(4002177, 1, customObj_GossipMenu)

function customObj_OnSelect(event, player, object, sender, intid, code)
	local clickedKicker = object:GetGUIDLow()
	local PlrName = player:GetName()
	local HouseOwner = WorldDBQuery("SELECT PlayerName FROM public_houses WHERE KickerGuid='" .. clickedKicker .. "'"):GetString(0)
	if (intid == 1) then
		local Range = 9
		for k,v in pairs (GetPlayersInWorld()) do
			if v:GetName() == PlrName then
				player:GossipComplete()
			elseif v:GetName():lower() == code:lower() and (player:IsWithinDistInMap(v, Range)) then
				--v:Teleport(169, 3434.04, -1232.80, 92.00+2, 3.14)
				v:Teleport(169, 3388.73,  -1234.69, 94.00, 0)
				v:SendNotification(""..HouseOwner.." Kicked Your Ass Out..")
			end
		end
	end
	if (intid == 1000) then
		player:GossipComplete()
	end
end

RegisterGameObjectGossipEvent(4002177, 2, customObj_OnSelect)