/*
**********************************
*      BotCore Custom Boss       *
*   	   Boss Wixar  			 *
*      Type: Boss Encounter		 *
*         By Josh Carter         *
**********************************
*/

/* ScriptData
SDCategory: Karazhan
SDName: Boss_Wixar
SD%Complete: 100%
SDComment: Needs in game test
EndScriptData
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "boss_wizards.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum Yells
{
	SAY_AGGRO									= 0, // 0 == Group ID for Creature_Text Table -> Sound included.
	SAY_PATHETIC								= 1,
	SAY_SLAY									= 2,
};

class boss_wixar : public CreatureScript
{
public:
	boss_wixar() : CreatureScript("boss_wixar") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new boss_wixarAI (creature);
	}

	struct boss_wixarAI : public ScriptedAI
	{
		boss_wixarAI(Creature* creature) : ScriptedAI(creature) {}

		uint32 SinfulbeamTimer;
		uint32 ChainlightningTimer;
		uint32 MoonfireTimer;
		uint32 EnrageTimer;

		void Reset()
		{
			SinfulbeamTimer       = urand(45000, 65000);
			ChainlightningTimer   = urand(25000, 85000);
			MoonfireTimer         = urand(30000, 75000);
			EnrageTimer			  = 600000;	// 10 min Enrage Timer
		}

		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			Talk(SAY_PATHETIC);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			Talk(SAY_SLAY);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			Talk(SAY_AGGRO);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
			if (!UpdateVictim())
				return;

			if (EnrageTimer <=diff && !EnrageTimer)
			{
				DoCast(me, SPELL_FRENZY);
				DoCast(me, SPELL_MASTER_BUFF_MELEE);
				DoCast(me, SPELL_MASTER_BUFF_MAGIC);
				DoCast(me, SPELL_NATURE_ALIGNED);
			}
			else
				EnrageTimer -= diff;

			if (SinfulbeamTimer <= diff)
			{
				DoCastVictim(SPELL_SINFUL_BEAM);
				me->MonsterYell("I will destroy all!!!", LANG_UNIVERSAL, me);
				SinfulbeamTimer = urand(45000, 65000);
			} else SinfulbeamTimer -= diff;

			if (ChainlightningTimer <= diff)
			{
				DoCast(me, SPELL_BOOST_CHAINLIGHTNING, true);
				DoCastVictim(SPELL_CHAINLIGHTNING);
				me->MonsterYell("Lighting will devour your flesh!!!", LANG_UNIVERSAL, me); // Look up in creature text for sound ID
				ChainlightningTimer = urand(25000, 85000);
			} else ChainlightningTimer -= diff;

			if (MoonfireTimer <= diff)
			{
				DoCast(me, SPELL_INCREASE_NATURE_DAMAGE, true);
				DoCastVictim(SPELL_MOONFIRE);
				me->MonsterYell("The Power of the moon and stars will destroy you!!!!", LANG_UNIVERSAL, me);
				MoonfireTimer = urand(30000, 75000);
			} else MoonfireTimer -= diff;

			DoMeleeAttackIfReady();
		}
	};
};

void AddSC_boss_wixar()
{
	new boss_wixar();
}