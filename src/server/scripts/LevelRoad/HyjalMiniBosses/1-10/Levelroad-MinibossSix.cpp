/*
**********************************
*      Icarus-Gaming Inc.        *
*   MiniBossSix Level Road IA    *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 60 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"

enum MiniBossSpells
{
SPELL_FORSAKEN_SKILL						= 7053, // Curses an enemy for 5 min., periodically reducing one of its skills by 100.
SPELL_SHADOW_BOLT							= 11659, // Sends a shadowy bolt at the enemy, causing 281 - 315 Shadow damage.
SPELL_DARK_SLASH							= 48292, // Slashes the target with darkness, dealing damage equal to half the target's current health.
SPELL_DARK_SPIN								= 30508, // Deals 275 weapon damage to enemies within range of the caster.
SPELL_DARK_WEAPON							= 49715, // Strikes at an enemy, inflicting 250 shadow damage.
};

class npc_minibosssix : public CreatureScript
{
public:
	npc_minibosssix() : CreatureScript("npc_minibosssix") { }

	CreatureAI* GetAI(Creature* creature) const OVERRIDE
	{
		return new npc_minibosssixAI (creature);
	}

	struct npc_minibosssixAI : public ScriptedAI
	{
		npc_minibosssixAI(Creature* creature) : ScriptedAI(creature) {}
		
			uint32 TalkTimerOne;
			uint32 TalkTimerTwo;
			uint32 TalkTimerThree;
			uint32 ForsakenSkillTimer;
			uint32 ShadowBoltTimer;
			uint32 DarkSlashTimer;
			uint32 DarkSpinTimer;
			uint32 DarkWeaponTimer;
			bool MiniBossInCombat;
							
		void Reset()
		{
			TalkTimerOne = urand(35000, 40000); // 35 to 40 seconds
			TalkTimerTwo = urand(15000, 30000); // 15 to 30 seconds
			TalkTimerThree = urand(45000, 60000); // 45 to 60 seconds
			ForsakenSkillTimer = urand(45000, 60000);
			ShadowBoltTimer = 20000;
			DarkSlashTimer = 30000;
			DarkSpinTimer = 40000;
			DarkWeaponTimer = 35000; 
			MiniBossInCombat = false;
			me->SetUInt32Value(UNIT_VIRTUAL_ITEM_SLOT_ID+0, 30313); // Staff of Disintegration
		}
		
		void KilledUnit(Unit* /*Victim*/) OVERRIDE
		{
			me->MonsterYell("Darkness takes the weak!!!", LANG_UNIVERSAL, me);
		}

		void JustDied(Unit* /*killer*/) OVERRIDE
		{
			me->MonsterYell("Ahhh... the sweet taste of death... I can finally rest...", LANG_UNIVERSAL, me);
			MiniBossInCombat = false;
			me->RemoveAllAuras();
			SetCombatMovement(false);
		}

		void EnterCombat(Unit* /*who*/) OVERRIDE
		{
			me->MonsterYell("Darkness and Death will consume you all!!!", LANG_UNIVERSAL, me);
			MiniBossInCombat = true;
			SetCombatMovement(true);
		}

		void UpdateAI(uint32 diff) OVERRIDE
		{
						
			if(!MiniBossInCombat)
			{	
				if (TalkTimerOne <= diff)
					{
					me->MonsterSay("I grow so weary of all this standing around!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerOne = urand(30000, 90000);
					} 
				else TalkTimerOne -= diff;
				
				if (TalkTimerTwo <= diff)
					{
					me->MonsterSay("All of this land will perish under the Soul Thief's rule!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerTwo = urand(40000, 60000); 
					} 
				else TalkTimerTwo -= diff;

				if (TalkTimerThree <= diff)
					{
					me->MonsterSay("We need more hero's around for me to slay!!!", LANG_UNIVERSAL, 0); // Change this to change what NPC says
					TalkTimerThree = urand(25000, 80000); 
					} 
				else TalkTimerThree -= diff;
						
			
			if (MiniBossInCombat)
				{
			
				if (!UpdateVictim())
					return;	
			
				if (ForsakenSkillTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(Target, SPELL_FORSAKEN_SKILL, true);
						me->MonsterYell("Your mind will addle and your skill will leave you!!!", LANG_UNIVERSAL, me);
						ForsakenSkillTimer = urand(30000, 45000);
						}
					}
				else ForsakenSkillTimer -= diff;
				
				if (ShadowBoltTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0))
						{
						DoCast(me, SPELL_SHADOW_BOLT, true);
						me->MonsterYell("Feel my shadow powers rend your body tiny mortal!!!", LANG_UNIVERSAL, me);
						ShadowBoltTimer = urand(50000, 65000);
						}
					}
				else ShadowBoltTimer -= diff;
					
				if (DarkSlashTimer <= diff)
					{
						DoCast(me->GetVictim(), SPELL_DARK_SLASH, true);
						me->MonsterYell("Pitiful MeatShield, I'll take you down a peg!!!", LANG_UNIVERSAL, me);
						DarkSlashTimer = urand(30000, 45000);
						}
					}
				else DarkSlashTimer -= diff;
									
				if (DarkSpinTimer <= diff)
					{
						DoCast(me, SPELL_DARK_SPIN, true);
						me->MonsterYell("All shall fall under my spinning Staff!!!", LANG_UNIVERSAL, me);
						DarkSpinTimer = urand(45000, 60000);
					}
				else DarkSpinTimer -= diff;
					
				if (DarkWeaponTimer <= diff)
					{
					if (Unit *Target = SelectTarget (SELECT_TARGET_RANDOM, 0)) 
						{
						DoCast(Target, SPELL_DARK_WEAPON, true);
						me->MonsterYell("Feel the force of my, Staff of Disintegration!!!", LANG_UNIVERSAL, me);
						DarkWeaponTimer = urand(25000, 70000);
						}
					}
					else DarkWeaponTimer -= diff;
				DoMeleeAttackIfReady();	
			}
		
		}
	};
};

void AddSC_npc_minibosssix()
{
	new npc_minibosssix();
}		