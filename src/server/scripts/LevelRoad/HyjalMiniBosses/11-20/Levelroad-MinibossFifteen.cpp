/*
**********************************
*      Icarus-Gaming Inc.        *
* MiniBossFifteen Level Road IA  *
*    Type: LevelRoad MiniBoss    *
*         By Josh Carter         *
**********************************
*/

/*
************************
* SD NOTES:            * 
* Level Road Mini-Boss *
* Level 150 -> 5man     *
* ??% Complete         *
************************
*/

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "PassiveAI.h"
#include "Player.h"
#include "SpellInfo.h"